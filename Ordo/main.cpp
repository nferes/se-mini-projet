#include <iostream>
#include <string>
#include <iomanip>
#include <fstream>

using namespace std;

bool verififinie(int (tab)[][6], int L) // Mise en évidence facultative mais très pédagogique de la portée locale de la variable.
{
    for(int i=0;i<L;i++)
    {
        if (tab[i][4] == 1)
        {
            return 1;
        }

    }
    return 0;
}

bool veriex(int (ta)[][6], int comp, int L) // Mise en évidence facultative mais très pédagogique de la portée locale de la variable.
{
    for(int i=0;i<L;i++)
    {
        if (ta[i][1] <= comp)
        {
            return 1;
        }

    }
    return 0;
}
int choose(int (ta)[][6], int comp, int L)
{
    int a = 4000;
    int ret = 0;
    for(int i=0; i<L; i++)
    {
        if(ta[i][1]<=comp)
        {
            if(ta[i][3]<a && ta[i][4] == 1)
            {
                a = ta[i][3];
                ret = i;
            }
        }
    }
    return ret;
}

int main(){

int comp = 0, RAM, hyst = 0;
int L,C;
//cout << "                        Scheduling projects for a startup"<<endl;
//cout << "                This project helps our startup to order the projects"<<endl;
/*cout << "            The idea is to always complete every project before the delay"<<endl;
cout << endl;
cout << endl;
cout << endl;
cout << endl;
cout << endl;
cout << "How many Projects the startup has please"<<endl;*/

ifstream inFile;

    inFile.open("/home/feres/Desktop/se-mini-projet/Ordo/test.txt");
    if (!inFile) {
        cout << "Unable to open file";
         // terminate with error
    }
    while (inFile){

    inFile >> L;

int tab[L][6];
    for(int i=0; i<L; i++)
    {
        tab[i][0] = i;
       // cout<< "Enter the day when the "<<(i+1) <<"th project came "<<endl;
        inFile >> tab[i][1];
        //cout <<"Enter approximately how many days needed to complete this project  "<<endl;
        inFile>> tab[i][2];
        //cout << "What is the day which we have to deliver this project"<< endl;
        inFile >> tab[i][3];
        tab[i][4] = 1;
        //cout << "How much do we gain from this project ?"<<endl;
        inFile >> tab[i][5];
    }
    while(verififinie(tab,L) == 1)
    {

        if(veriex(tab, comp, L) == 1)
        {
            RAM = choose(tab, comp, L);
            hyst = comp;
            comp = comp + tab[RAM][2];
            tab[RAM][4] = 0;
            if(tab[RAM][3]<comp)
            {
                cout<<"Process number "<<RAM+1<< " started at "<<hyst<<", it ended late at  "<<comp<<", but with the minimum of damage" <<endl ;
            }
            else cout<<"Process number "<<RAM+1<< " started at "<<hyst <<" it ended at "<<comp<<endl;
        }
        else    {
                comp++;
                cout<<"No process yet at "<<comp<<endl;
                }

          std::cin.ignore();

    }
    }
inFile.close();



	return 0;
}
