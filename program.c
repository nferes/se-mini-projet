#include<stdio.h> 
#include<string.h> 
#include<stdlib.h>
#include<readline/readline.h> 
#include<readline/history.h> 
#include <mcheck.h>
#include <unistd.h>
#include <dirent.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <time.h>
#include <pwd.h> 

#define DATA_SIZE 1000
#define BUFFERSIZE 200
#define LSIZ 128 
#define RSIZ 10 

void printFile (char* filename,char *content){
   FILE * fp;
   int i;
   char* file;
   char* c;
   file=("%s",filename);
   /* open the file for writing*/
   fp = fopen (file,"w");
   
   /* write 10 lines of text into the file stream*/
       fprintf (fp,"%s", content);

 
   /* close the file*/  
   fclose (fp);

   puts("content in file");
}


void fileParser(char* filename,char* commands[]) {
    char line[RSIZ][LSIZ];
    FILE *fptr = NULL; 
    int i = 0;
    int tot = 0;
    
    printf("\n\n Read the file and store the lines into an array :\n");
	printf("------------------------------------------------------\n"); 

    fptr = fopen(filename, "r");
    while(fgets(line[i], LSIZ, fptr)) 
	{
        line[i][strlen(line[i]) - 1] = '\0';
        i++;
    }
    tot = i;   
    for(i = 0; i < tot; ++i)
    {
        commands[i]=line[i];
        
    }
    printf("\n");
    

    }



char* substr(const char *src, int m, int n){
	// get length of the destination string
	int len = n - m;

	// allocate (len + 1) chars for destination (+1 for extra null character)
	char *dest = (char*)malloc(sizeof(char) * (len + 1));

	// extracts characters between m'th and n'th index from source string
	// and copy them into the destination string
	for (int i = m; i < n && (*src != '\0'); i++)
	{
		*dest = *(src + i);
		dest++;
	}

	// null-terminate the destination string
	*dest = '\0';

	// return the destination string
	return dest - len;
}
char* readFile () {
   FILE *fp;
   char* ptr;
   char ch[60];
   char* solution="";

   /* opening file for reading */
   fp = fopen("profile.txt" , "r");
   if(fp == NULL) {
      perror("Error opening file");
   }
   if( fgets (ch, 60, fp)!=NULL ) {
      /* writing content to stdout */
      puts(ch);
   }
   fclose(fp);
   ptr=strchr(ch,'=');
   int index=ptr-ch+1;
    solution=substr(ch,index,sizeof(ch));
    return solution;
}

void do_ls( char dirname[] ){
        DIR             *dir_ptr;               /* the directory */
        struct dirent   *direntp;               /* each entry    */

        if ( ( dir_ptr = opendir( dirname ) ) == NULL )
                fprintf(stderr,"ls1: cannot open %s\n", dirname);
        else
        {
                while ( ( direntp = readdir( dir_ptr ) ) != NULL )
                        printf("%s\n", direntp->d_name );
                closedir(dir_ptr);
        }
}

char *pwd (){
  char *dir;
  /* Let this program be used for debugging.  */
  mtrace ();
  dir = getcwd ((char *) NULL, 0);
  if (dir == NULL)
    perror ("getcwd");
  else
    {
      puts (dir);
      free (dir);
    }
  return dir;
}


char* profile() 
{
    char line[RSIZ][LSIZ];
	
    FILE *fptr = NULL; 
    int i = 0;
    char* ptr;
    int tot = 0;
    char * path;
    char * home ;	
    char * result;
    fptr = fopen("profile.txt", "r");
    while(fgets(line[i], LSIZ, fptr)) 
	{
        line[i][strlen(line[i]) - 0] = '\0';
        i++;
    }
    tot = i;    
    path=line[0];
    ptr=strchr(line[1],'=');
   int index=ptr-line[1]+1;
    home=substr(line[1],index,sizeof(line[1]));
    return home;
    }

int cdir(char *pth){
    char path[BUFFERSIZE];
    strcpy(path,pth);

    char cwd[BUFFERSIZE];
    if(pth[0] != '/')
    {// true for the dir in cwd
        getcwd(cwd,sizeof(cwd));
        strcat(cwd,"/");
        strcat(cwd,path);
        chdir(cwd);
    }else{//true for dir w.r.t. /
        chdir(pth);
    }

    return 0;
}

char* takeInput() { 
    char* buf; 
    buf = readline(">>> "); 
    return buf;
} 

int main(int argc,char* argv[]){
char* ptr;
char* command=(char*)malloc(10*sizeof(char));
char* command2=(char*)malloc(10*sizeof(char));
char* filename=(char*)malloc(10*sizeof(char));
char* ls=(char*)malloc(10*sizeof(char));
char* arg=(char*)malloc(10*sizeof(char));
char* cd=(char*)malloc(10*sizeof(char));
char* path=(char*)malloc(10*sizeof(char));
char* whereami=(char*)malloc(10*sizeof(char));
char* PATH=(char*)malloc(10*sizeof(char));
char* HOME=(char*)malloc(10*sizeof(char));
char* alias=(char*)malloc(10*sizeof(char));
char* test=(char*)malloc(10*sizeof(char));
char* countL=(char*)malloc(10*sizeof(char));
char *ar[50];
ls="showlist";
cd="goto";
whereami="whereami";
alias="alias";
PATH="path";
HOME="home";
countL="countL";
if(argc >= 2){
            printf("thers is an argument %s\n",argv[1]);
            fileParser(argv[1],ar);
            printf("The command %s\n",ar[0]);
            do_ls(".");
            puts("Next:\n");
            printf("The command %s\n",ar[1]);
            readFile();
            puts("Next:\n");
            printf("The command whereami\n");
            pwd();
            puts("Next:\n");
            printf("The command home\n");
            puts(profile());
            puts("Have a good day\n");
            exit(0);
        }else{
             printf("Welcome to our shell project\n");
	     printf("Here you can use this list of commands\n");
             printf("-whereami: to show the current path\n");
             printf("-showlist: the job of ls\n");
             printf("-goto: to change the current directory\n");
             printf("-createfile: the job of nano\n");
             printf("-createdir: to create a new directory\n");
	     printf("-alias: to change the commands name\n");
             printf("->: the redirection command\n");
             printf("-|: pipe\n");
             printf("-countL : to count the output lines\n");
             printf("-path: to show the data path\n");
             printf("-home: to show the home path\n");
             printf("-exit: to close the temrinal\n");
            while(1){
            char* buffer=takeInput();

            if(strstr(buffer,"alias")){
                char* cmd=strndup(buffer+strlen(alias)+1,500);
                if(strstr(cmd,"=")){
                   
                    char* c=strchr(cmd,'=');
                    int cpt=c-cmd+1   ;
                    
                    char* oldcmd=substr(cmd,0,cpt-1);
                    char* newcmd=substr(cmd,cpt,strlen(cmd));
                    puts(newcmd);
                    puts(oldcmd);
                    if(strcmp(oldcmd,"goto")==0){
                        cd=newcmd;
                        puts(newcmd);
                        puts(cd);
                    }else if(strcmp(oldcmd,"whereami")==0){
                        whereami=newcmd;
                    }
                    else if(strcmp(oldcmd,"lister")==0){
                        ls=newcmd;
                    }
                    else if(strcmp(oldcmd,"home")==0){
                        HOME=newcmd;
                    }
                    else if(strcmp(oldcmd,"path")==0){
                        PATH=newcmd;
                    }
                }
            }
            else if(strstr(buffer,"exit")){
       		exit(0);
            }

            else if(strstr(buffer,ls)){
            arg=strndup(buffer+strlen(ls)+1,500);
            printf("%d",(int)strlen(arg));
            if(strlen(arg)!=0){
                do_ls(arg);
            } else {
                do_ls(".");
            }      
            } else if(strcmp(buffer,whereami)==0){
                pwd();
            }  else if(strcmp(buffer,"createfile")==0){
                 system("./NANO/touchh");
	    }  else if(strcmp(buffer,"createdir")==0){
                 system("./MKDIR/mmkdir");
            }  else if(strstr(buffer,">")){

		ptr=strchr(buffer,'>');
                int index=ptr-buffer+1;
                printf("found it at %d\n",index);
                command=substr(buffer,0,index-1);
                puts(command);
                filename=substr(buffer,index,strlen(buffer));
                puts(filename);
                if(strcmp("chemin",command)==0){
                    printFile(filename,getcwd ((char *) NULL, 0));
                }
 	    }
            else if(strstr(buffer,"|")){
            ptr=strchr(buffer,'|');
            int index=ptr-buffer+1;
            command=substr(buffer,0,index-1);
            command2=substr(buffer,index,strlen(buffer));
            if(strcmp(whereami,command)==0 && strcmp(countL,command2)==0) {
            	  
                  system("pwd |wd -c");
		}
            else printf("we do not support %s command or %s command yet, try later",command,command2); 
            }
            else if(strstr(buffer,cd)){
                path=strndup(buffer+strlen(cd)+1,500);
                puts(path);
                cdir(path);
            }else if(strcmp(buffer,HOME)==0){
                puts(profile());
            }else if(strcmp(buffer,PATH)==0){
                readFile();
            } 
               
            }
        }
}
