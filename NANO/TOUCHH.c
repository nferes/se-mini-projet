/**
 * C program to create a file and write data into file.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define DATA_SIZE 1000

int main()
{
    /* Variable to store user content */
    char data[DATA_SIZE];
    char path[DATA_SIZE];

    /* File pointer to hold reference to our file */
    FILE * fPtr;

    printf("enter the lication and the name of the file you want to create: \n");
    fgets(path, DATA_SIZE, stdin);
    /* 
     * Open file in w (write) mode. 
     * "data/file1.txt" is complete path to create file
     */
    fPtr = fopen(path, "w");


    /* fopen() return NULL if last operation was unsuccessful */
    if(fPtr == NULL)
    {
        /* File not created hence exit */
        printf("Unable to create file.\n");
        exit(EXIT_FAILURE);
    }


    /* Input contents from user to store in file */
    printf("Enter contents to store in file : \n");
    fgets(data, DATA_SIZE, stdin);


    /* Write data to file */
    fputs(data, fPtr);


    /* Close file to save file data */
    fclose(fPtr);


    /* Success message */
    printf("File created and saved successfully. 🙂 \n");


    return 0;
}
